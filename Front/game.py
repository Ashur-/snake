import pygame
import random
import pygame.mixer
import pickle

pygame.init()

WIDTH, HEIGHT = 800, 600
spider_SIZE = 20
spider_SPEED = 10

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
COBALT = (0, 71, 171)
PINK = (255, 105, 180)
RAINBOW = [(255, 0, 0), (255, 165, 0), (255, 255, 0), (0, 128, 0), (0, 0, 128), (75, 0, 130), (148, 0, 211)]

window = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Spider Game")

background_image = pygame.image.load("BG3.jpg")
background_image = pygame.transform.scale(background_image, (WIDTH, HEIGHT))

clock = pygame.time.Clock()

font = pygame.font.Font(None, 36)
high_score_font = pygame.font.Font(None, 36)

def draw_spider(spider, rotated_spider):
    for segment in spider:
        window.blit(rotated_spider, (segment[0], segment[1]))

def generate_food(spider):
    while True:
        food = (random.randint(0, (WIDTH - spider_SIZE) // spider_SIZE) * spider_SIZE,
                random.randint(0, (HEIGHT - spider_SIZE) // spider_SIZE) * spider_SIZE)
        if food not in spider:
            return food

spider = [(WIDTH // 2, HEIGHT // 2)]
spider_dir = (0, 0)
food = generate_food(spider)
food_color = PINK
score = 0
game_over = False
game_over_message = "Game Over. Press Enter to retry."
game_over_state = False
rainbow_index = 0

pygame.mixer.init()

pygame.mixer.music.load('Rhapsody.mp3')

pygame.mixer.music.set_volume(0.5)

pygame.mixer.music.play(-1)

high_score = 0

try:
    with open("highscore.dat", "rb") as file:
        high_score = pickle.load(file)
except FileNotFoundError:
    high_score = 0

spider_image = pygame.image.load("spider.png")
spider_image = pygame.transform.scale(spider_image, (spider_SIZE, spider_SIZE))

beetle_image = pygame.image.load("beetle.png")
beetle_image = pygame.transform.scale(beetle_image, (spider_SIZE, spider_SIZE))

spider_direction = "north"

while not game_over:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            game_over = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP and spider_dir != (0, 1):
                spider_dir = (0, -1)
                spider_direction = "north"

            if event.key == pygame.K_DOWN and spider_dir != (0, -1):
                spider_dir = (0, 1)
                spider_direction = "south"

            if event.key == pygame.K_LEFT and spider_dir != (1, 0):
                spider_dir = (-1, 0)
                spider_direction = "west"

            if event.key == pygame.K_RIGHT and spider_dir != (-1, 0):
                spider_dir = (1, 0)
                spider_direction = "east"

            if game_over_state and event.key == pygame.K_RETURN:
                game_over_state = False
                game_over_message = "Game Over. Press Enter to retry."
                spider = [(WIDTH // 2, HEIGHT // 2)]
                spider_dir = (0, 0)
                food = generate_food(spider)
                food_color = PINK
                score = 0

    new_head = (spider[0][0] + spider_dir[0] * spider_SIZE, spider[0][1] + spider_dir[1] * spider_SIZE)
    spider.insert(0, new_head)

    if spider[0] == food:
        score += 1
        food = generate_food(spider)
    else:
        spider.pop()

    if (spider[0][0] < 0 or spider[0][0] >= WIDTH or
            spider[0][1] < 0 or spider[0][1] >= HEIGHT or
            spider[0] in spider[1:]):
        game_over_state = True

    window.blit(background_image, (0, 0))

    if spider_direction == "north":
        rotated_spider = spider_image
    elif spider_direction == "south":
        rotated_spider = pygame.transform.rotate(spider_image, 180)
    elif spider_direction == "west":
        rotated_spider = pygame.transform.rotate(spider_image, 90)
    elif spider_direction == "east":
        rotated_spider = pygame.transform.rotate(spider_image, -90)

    draw_spider(spider, rotated_spider)

    window.blit(beetle_image, (food[0], food[1]))

    score_text = font.render(f"Score: {score}", True, BLACK)
    window.blit(score_text, (10, 10))

    high_score_text = high_score_font.render(f"High Score: {high_score}", True, BLACK)
    window.blit(high_score_text, (10, 50))

    if game_over_state:
        rainbow_index = (rainbow_index + 1) % len(RAINBOW)
        message_text = font.render(game_over_message, True, RAINBOW[rainbow_index])
        window.blit(message_text, (WIDTH // 2 - 180, HEIGHT // 5))

    if score > high_score:
        high_score = score

    pygame.display.update()

    clock.tick(spider_SPEED)

with open("highscore.dat", "wb") as file:
    pickle.dump(high_score, file)

pygame.quit()
